/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package cz.vse.java.adventura;



import cz.vse.java.adventura.Osoba;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída OsobaTest slouží ke komplexnímu otestování třídy ... 
 *
 * @author    jméno autora
 * @version   0.00.000
 */
public class OsobaTest
{
    //== KONSTRUKTORY A TOVÁRNÍ METODY =========================================
    //-- Testovací třída vystačí s prázdným implicitním konstruktorem ----------

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
        Osoba osoba1 = new Osoba("letuška", "V duty free shopu jsem zahlédla nějaký voucher.");
        Osoba osoba2 = new Osoba("pilot", "Dávejte si pozor, v některé z vedlejších místností je celník.");
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }

    //== VLASTNÍ TESTY =========================================================
    //
    //     /********************************************************************
    //      *
    //      */
    //     @Test
    //     public void testXxx()
    //     {
    //     }

    @Test
    public void OsobaTest1()
    {
        
        Osoba osoba1 = new Osoba("letuška", "V duty free shopu jsem zahlédla nějaký voucher.");
        Osoba osoba2 = new Osoba("pilot", "Dávejte si pozor, v některé z vedlejších místností je celník.");
        assertEquals("letuška", osoba1.vratJmeno());
        assertEquals("Dávejte si pozor, v některé z vedlejších místností je celník.", osoba2.vratRadu());
    }
}

