package cz.vse.java.adventura;



import static org.junit.Assert.*;

import cz.vse.java.adventura.HerniPlan;
import cz.vse.java.adventura.PrikazOdevzdej;
import cz.vse.java.adventura.Vec;
import cz.vse.java.adventura.Vozik;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class PrikazOdevzdejTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class PrikazOdevzdejTest
{
    /**
     * Default constructor for test class PrikazOdevzdejTest
     */
    public PrikazOdevzdejTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }

    @Test
    public void testPrikaz()
    {
        HerniPlan herniPla1 = new HerniPlan();
        PrikazOdevzdej prikazOd1 = new PrikazOdevzdej(herniPla1);
        Vozik vozik1 = herniPla1.vratVozik();
        Vec vec1 = new Vec("vec1", 12);
        vozik1.vlozVec(vec1);
        java.lang.String string1 = prikazOd1.provedPrikaz("vec1");
        assertEquals(string1, "Vec vec1 byla odevzdana");
    }
}

