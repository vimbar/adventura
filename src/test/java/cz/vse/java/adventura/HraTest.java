package cz.vse.java.adventura;

import cz.vse.java.adventura.Hra;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída HraTest slouží ke komplexnímu otestování
 * třídy Hra
 *
 * @author    Jarmila Pavlíčková
 * @version  pro školní rok 2016/2017
 */
public class HraTest {
    private Hra hra1;

    //== Datové atributy (statické i instancí)======================================

    //== Konstruktory a tovární metody =============================================
    //-- Testovací třída vystačí s prázdným implicitním konstruktorem ----------

    //== Příprava a úklid přípravku ================================================

    /***************************************************************************
     * Metoda se provede před spuštěním každé testovací metody. Používá se
     * k vytvoření tzv. přípravku (fixture), což jsou datové atributy (objekty),
     * s nimiž budou testovací metody pracovat.
     */
    @Before
    public void setUp() {
        
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každé testovací metody.
     */
    @After
    public void tearDown() {
    }

    //== Soukromé metody používané v testovacích metodách ==========================

    //== Vlastní testovací metody ==================================================

    /***************************************************************************
     * Testuje průběh hry, po zavolání každěho příkazu testuje, zda hra končí
     * a v jaké aktuální místnosti se hráč nachází.
     * Při dalším rozšiřování hry doporučujeme testovat i jaké věci nebo osoby
     * jsou v místnosti a jaké věci jsou v batohu hráče.
     * 
     */
    @Test
    public void testPrubehHry() {
        
    }

    @Test
    public void HraTest1()
    {
        Hra hra1 = new Hra();
        assertEquals(true, hra1.getHerniPlan());
        assertEquals(true, hra1.vratEpilog());
        assertEquals(true, hra1.vratUvitani());
        assertEquals(false, hra1.zpracujPrikaz("jdi"));
        assertEquals(true, hra1.zpracujPrikaz("jdi toalety"));
        assertEquals(true, hra1.zpracujPrikaz("jdi checkin"));
        assertEquals(false, hra1.konecHry());
    }

    @Test
    public void testUspesneHry()
    {
        Hra hra1 = new Hra();
        hra1.zpracujPrikaz("jdi odbavovna");
        hra1.zpracujPrikaz("seber pas");
        hra1.zpracujPrikaz("jdi uschovna");
        hra1.zpracujPrikaz("jdi shop");
        hra1.zpracujPrikaz("seber letenka");
        hra1.zpracujPrikaz("jdi toalety");
        hra1.zpracujPrikaz("seber kufr");
        hra1.zpracujPrikaz("jdi checkin");
        hra1.zpracujPrikaz("odevzdej pas");
        hra1.zpracujPrikaz("jdi uschovna");
        hra1.zpracujPrikaz("jdi odbavovna");
        hra1.zpracujPrikaz("odevzdej kufr");
        hra1.zpracujPrikaz("jdi gate1");
        hra1.zpracujPrikaz("jdi gate2");
        hra1.zpracujPrikaz("jdi gate3");
        hra1.zpracujPrikaz("odevzdej letenka");
     
        assertEquals(true, hra1.konecHry());
    }
}


