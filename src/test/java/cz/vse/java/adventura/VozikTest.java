/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package cz.vse.java.adventura;



import cz.vse.java.adventura.Vec;
import cz.vse.java.adventura.Vozik;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída VozikTest slouží ke komplexnímu otestování třídy ... 
 *
 * @author    Barbora Vimmerová
 * @version   0.00.000
 */
public class VozikTest
{
    //== KONSTRUKTORY A TOVÁRNÍ METODY =========================================
    
    //-- Testovací třída vystačí s prázdným implicitním konstruktorem ----------

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
        Vozik vozik1 = new Vozik();
        Vec vec1 = new Vec("kufr", 18);
        Vec vec2 = new Vec("pas", 2);
        Vec vec3 = new Vec("letenka", 2);
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }

    //== VLASTNÍ TESTY =========================================================
    //
    //     /********************************************************************
    //      *
    //      */
    //     @Test
    //     public void testXxx()
    //     {
    //     }

    @Test
    public void VozikTest()
    {
        Vozik vozik1 = new Vozik();
        Vec vec1 = new Vec("kufr", 18);
        Vec vec2 = new Vec("pas", 2);
        Vec vec3 = new Vec("letenka", 2);
        assertEquals(true, vozik1.vlozVec(vec1));
        assertEquals(true, vozik1.vlozVec(vec2));
        assertEquals(false, vozik1.vlozVec(vec2));
        assertEquals(false, vozik1.vlozVec(vec3));
        assertEquals(true, vozik1.vlozVec(vec3));
    }
}

