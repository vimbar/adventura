package cz.vse.java.adventura;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.net.URL;

public class Controller {

    public static final int SIRKA_IKONY = 45;
    public static final int VYSKA_IKONY = 30;

    @FXML
    private VBox seznamVychodu;

    @FXML
    private VBox seznamPredmetuVMistnosti;

    @FXML
    private VBox seznamPredmetuVeVoziku;

    private IHra hra;

    public ImageView obrazekLokace;

    @FXML
    private Label popisLokace;

    @FXML
    private Label jmenoLokace;

    @FXML
    private Button napoveda;

    @FXML
    private Button novaHra;

    /**
     * Tato metoda nastavuje tlačítka ve hře. Tlačítko "Nápověda", spustí event otevření
     * nového okna s nápovědou v HTML. Tlačítko "Nová hra" smaže všechny věci z vozíku
     * a započne nová hra.
     */
    public void setMenu() {
        napoveda.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                WebView browser = new WebView();
                WebEngine webEngine = browser.getEngine();
                URL url = this.getClass().getResource("/napoveda.html");
                webEngine.load(url.toString());

                Stage stage = new Stage();
                stage.setTitle("Nápověda");
                stage.setScene(new Scene(browser, 450, 450));
                stage.show();
            }
        });

        novaHra.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                smazVeciZVoziku();
                setHra(new Hra());

            }
        });
    }

    /**
     * Tato metoda inicializuje novou hru. Přiřazuje hře
     * herní plán a nastavuje aktuální prostor.
     * @param hra
     */
    public void setHra(IHra hra) {
        this.hra = hra;
        HerniPlan herniPlan = hra.getHerniPlan();
        Prostor aktualniProstor = herniPlan.vratAktualniProstor();
        zmenProstor(aktualniProstor);
    }

    /**
     * Tato metoda zjistí aktuální prostor a podle něj zobrazí na stagi
     * název prostoru, jeho obrázek a popis. Dále zavolá metodu pridejVychody a
     * vypíše východy z prostoru a předměty, které se v něm nachází.
     */
    private void zmenProstor(Prostor prostor) {
        hra.zpracujPrikaz("jdi " + prostor.getNazev());
        System.out.println(hra.getHerniPlan().vratAktualniProstor().getNazev());

        jmenoLokace.setText(prostor.getNazev());
        popisLokace.setText(prostor.dlouhyPopis());

        String nazevObrazku = "/" + prostor.getNazev() + ".jpg";
        Image image = new Image(getClass().getResourceAsStream(nazevObrazku));
        obrazekLokace.setImage(image);

        pridejVychody(prostor);
        pridejPredmety(prostor);
    }

    /**
     * Tato metoda přidává východy a jejich obrázky do HBoxu podle toho, který prostor je
     * v danou chvíli aktuální. Nastavuje velikost obrázků a určuje, že
     * pokud kliknu na jeden z východů, změní se aktuální prostor na tento
     * východ.
     */
    private void pridejVychody(Prostor prostor) {
        seznamVychodu.getChildren().clear();
        for (Prostor p : prostor.getVychody()) {
            HBox vychod = new HBox();
            vychod.setSpacing(10);
            Label nazevProstoru = new Label(p.getNazev());

            ImageView vychodImageView = new ImageView();
            Image vychodImage = new Image(getClass().getClassLoader().getResourceAsStream( p.getNazev() + ".jpg"));
            vychodImageView.setFitHeight(VYSKA_IKONY);
            vychodImageView.setFitWidth(SIRKA_IKONY);
            vychodImageView.setImage(vychodImage);


            vychod.getChildren().addAll(vychodImageView, nazevProstoru);

            seznamVychodu.getChildren().add(vychod);
            vychod.setOnMouseClicked(event -> {
                zmenProstor(p);
            });
        }
    }

    /**
     * Metoda přidá do místnosti všechny předměty definované pro daný prostor
     */

    private void pridejPredmety(Prostor prostor) {
        seznamPredmetuVMistnosti.getChildren().clear();

        for (Vec vec : prostor.getVeci().values()) {
            pridejPredmetDoMistnosti(vec);
        }
    }

    /**
     * Metoda zajišťuje přesouvání věcí mezi vozíkem a místností.
     * Pokud kliknu na věc v seznamu věcí v místnosti,
     * tak se věc přidá do vozíku, pakliže bude hmotnost věci menší než 2Okg.
     * Když kliknu na věc ve vozíku, tak se předmět naopak přidá
     *do seznamu věcí v místnosti.
     */

    private void pridejPredmetDoMistnosti(Vec vec) {
        Label nazevVeci = new Label(vec.vratNazev());

        ImageView vecImageView = new ImageView();
        Image vecImage = new Image(getClass().getClassLoader().getResourceAsStream( vec.vratNazev() + ".jpg"));
        vecImageView.setFitHeight(VYSKA_IKONY);
        vecImageView.setFitWidth(SIRKA_IKONY);
        vecImageView.setImage(vecImage);

        seznamPredmetuVMistnosti.getChildren().addAll(vecImageView, nazevVeci);

        int celkovaHmotnost = hra.getHerniPlan().vratVozik().spocitejHmotnost();

        nazevVeci.setOnMouseClicked(event -> {

            if((vec.vratHmotnost() + celkovaHmotnost) <= 20)
            {
                hra.zpracujPrikaz("seber " + vec.vratNazev());
                Label vecVeVoziku = new Label(vec.vratNazev());
                ImageView vecVeVozikuImageView = new ImageView();
                Image vecVeVozikuImage = new Image(getClass().getClassLoader().getResourceAsStream( vec.vratNazev() + ".jpg"));
                vecVeVozikuImageView.setFitHeight(VYSKA_IKONY);
                vecVeVozikuImageView.setFitWidth(SIRKA_IKONY);
                vecVeVozikuImageView.setImage(vecVeVozikuImage);
                seznamPredmetuVeVoziku.getChildren().addAll(vecVeVozikuImageView, vecVeVoziku);
                seznamPredmetuVMistnosti.getChildren().removeAll(nazevVeci,vecImageView);

                vecVeVoziku.setOnMouseClicked(event1 -> {
                    hra.zpracujPrikaz("odevzdej "+vec.vratNazev());
                    seznamPredmetuVeVoziku.getChildren().removeAll(vecVeVoziku,vecVeVozikuImageView);
                    pridejPredmetDoMistnosti(vec);
                });

            }
        });
    }

    /**
     * Metoda odstraňuje všechny nasbírané věci z vozíku.
     */
    public void smazVeciZVoziku() {
        seznamPredmetuVeVoziku.getChildren().clear();
    }
}
