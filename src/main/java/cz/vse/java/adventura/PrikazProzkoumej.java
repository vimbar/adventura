/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package cz.vse.java.adventura;



/*******************************************************************************
 * Instance třídy PrikazProzkoumej představují ...
 *
 * @author    Barbora Vimmerová
 * @version   0.00.000
 */
public class PrikazProzkoumej implements IPrikaz
{
    //== Datové atributy (statické i instancí)======================================
    private static final String NAZEV = "prozkoumej";
    private HerniPlan plan;
    //== Konstruktory a tovární metody =============================================

    /***************************************************************************
    *  Konstruktor třídy
    *  
    *  @param plan herní plán, ve kterém se bude ve hře "chodit" 
    */   
    public PrikazProzkoumej(HerniPlan plan)
    {
         this.plan = plan;
    }
        
     /**
     *  Provádí příkaz "prozkomej.". Metoda popíše,jaké věci se v daném prostoru vyskytují
     *  a všechny dostupné informace o nich.
     *@return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String provedPrikaz(String... parametry) {
        Prostor aktualniProstor = plan.vratAktualniProstor();
        return aktualniProstor.popisVeci();
    }
    
    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }
}




