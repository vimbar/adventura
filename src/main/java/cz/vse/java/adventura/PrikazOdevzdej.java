/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package cz.vse.java.adventura;



/*******************************************************************************
 * Instance třídy PrikazOdevzdej představují ...
 *
 * @author    Barbora Vimmerová
 * @version   0.00.000
 */
public class PrikazOdevzdej implements IPrikaz
{
    //== Datové atributy (statické i instancí)======================================
    private static String NAZEV = "odevzdej";
    private HerniPlan plan;

    //== Konstruktory a tovární metody =============================================

    /**
    *  Konstruktor třídy
    *  
    *  @param plan herní plán, ve kterém se bude ve hře "chodit" 
    */    
    public PrikazOdevzdej(HerniPlan plan)
    {
        this.plan = plan;
    }

    //== Nesoukromé metody (instancí i třídy) ======================================
    
    /**
     *  Provádí příkaz "odevzdej". Tato metoda vyndá z 
     *  vozíku zadanou věc a dá ji do aktuálního prostoru, 
     *  pokud hráč nezadá název věci, bude informován, 
     *  aby napsal, co konkrétně chce odevzdat, stejně tak
     *  pokud takovou věc ve vozíku nemá, bude o tom uvědomen.
     *
     *@param parametry - jako  parametr obsahuje název věci,
     *                         kterou hce hráč odevzdat
     *@return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo , tak ....
            return "Co mám odevzdat? Musíš zadat název věci.";
        }

        String nazevVeci = parametry[0];
        Prostor aktualniProstor = plan.vratAktualniProstor();
        
        Vozik vozik = plan.vratVozik();
        Vec vec = vozik.vyndatVec(nazevVeci);
        if(vec == null) {
            return "Tuto vec nemas ve vozíku.";
        }
 
        aktualniProstor.vlozVec(vec);
        return "Vec " + vec.vratNazev() +" byla odevzdana";
    }
     
    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }
    


    //== Soukromé metody (instancí i třídy) ========================================

}
