/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package cz.vse.java.adventura;



/*******************************************************************************
 * Instance třídy Vec představují ...
 *
 * @author    Barbora Vimmerová

 * @version   0.00.000
 */
public class Vec
{
    //== Datové atributy (statické i instancí)======================================
    private String nazev;
    private int hmotnost;
    //== Konstruktory a tovární metody =============================================

    /***************************************************************************
     *  Konstruktor třídy 
     */
    public Vec(String nazev, Integer hmotnost)
    {
        this.nazev = nazev;
        this.hmotnost = hmotnost;
    }
    
    /**
     *  Metoda vrátí název věci.
     */ 
    public String vratNazev(){
        return nazev;
    }
    
    /**
     *  Metoda vrátí hmotnost věci.
     */ 
    public int vratHmotnost(){
        return hmotnost;
    }

    //== Nesoukromé metody (instancí i třídy) ======================================


    //== Soukromé metody (instancí i třídy) ========================================

}
