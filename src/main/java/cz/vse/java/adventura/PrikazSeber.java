/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package cz.vse.java.adventura;



/*******************************************************************************
 * Instance třídy PrikazSeber představují ...
 *
 * @author    Barbora Vimmerová
 * @version   0.00.000
 */
public class PrikazSeber implements IPrikaz
{
  
    private static final String NAZEV = "seber";
    private HerniPlan plan;
    //private Kosik kosik;
    
    /**
    *  Konstruktor třídy
    *  
    *  @param plan herní plán, ve kterém se bude ve hře "chodit" 
    */    
    public PrikazSeber(HerniPlan plan) {
        this.plan = plan;
       //vozik = plan.getVozik();
    }

    /**
     *  Provádí příkaz "seber". Pokud hrč nezadá parametr, 
     *  tedy věc, kterou chce sebrat, zeptá se ho hra znovu, 
     *  co přeesně chce sebrat. Pokud se věc v místnosti 
     *  nachází a hráč ji uveze,věc zmizí z prostoru hry a
     *  přidá se na seznam věcí ve vozíku, pokud věc hráč
     *  už nemůže přidat do vozíku nebo pokud v místnosti 
     *  taková věc není, vypíše se hráči důvod, proč věc
     *  nelze sebrat.
     *
     *@param parametry - jako  parametr obsahuje název věci,
     *                         kterou  má sebrat.
     *@return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo , tak ....
            return "Co mám sebrat? Musíš zadat název věci.";
        }
        String nazevSbirane = parametry[0];
        Prostor aktualniProstor = plan.vratAktualniProstor();
        Vec sbiranaVec = aktualniProstor.odeberVec(nazevSbirane);  

        if (sbiranaVec != null) {
            
            if(sbiranaVec.vratNazev().equals("voucher1") || sbiranaVec.vratNazev().equals("voucher2") || sbiranaVec.vratNazev().equals("voucher3")) {
                plan.zmenPocetNavstivenychProstoru(-5);
                return "Diky voucheru ti bylo odecteno 5 kroku.";
            }
            else if(plan.vratVozik().vlozVec(sbiranaVec)) {
                return "Věc " + sbiranaVec.vratNazev() +" byla vlozena do voziku";
            } else {
                aktualniProstor.vlozVec(sbiranaVec);
                return "Vec " + sbiranaVec.vratNazev() +" neuneses";
            }           
        }
        else {
            return "To tu není.";
        }
    }
    
    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }
}
