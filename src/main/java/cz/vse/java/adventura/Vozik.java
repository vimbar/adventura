/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package cz.vse.java.adventura;

import java.util.*;

/*******************************************************************************
 * Instance třídy Vozik představují ...
 *
 * @author    Barbora Vimmerová
 * @version   0.00.000
 */
public class Vozik
{
    //== Datové atributy (statické i instancí)======================================
    public Collection<Vec> seznamVeci;

    //== Konstruktory a tovární metody =============================================

    /***************************************************************************
     *  Konstruktor 
     *  nový seznam věcí, které jsou ve vozíku
     */
    public Vozik()
    {
        seznamVeci = new HashSet(); 
    }

    //== Nesoukromé metody (instancí i třídy) ======================================
    /**
     *  Metoda vloží věc do seznamu, pokud součet hmotnosti 
     *  této věci a věcí, které již v seznamu jsou nepřekročí
     *  dohromady limit 20kg. Pokud byla všěc vložena, vrátí true,
     *  jinak false.
     *
     *@param parametry - jako  parametr obsahuje název věci,
     *                         která má být vložena
     *                         
     *@return true or false
     */ 
    
      public boolean vlozVec(Vec vec){ 
          if((this.spocitejHmotnost() + vec.vratHmotnost()) <= 20)
          {
             seznamVeci.add(vec);
             return true;
          } else {
             return false;    
          }
      }
      
      /**
     *  Metoda vrací seznam věcí ve vozíku.
     */ 

    public Collection<Vec> vratSeznamVeci(){
        return seznamVeci;
    }

    /**
     *  Metoda prohledá seznam věcí ve vozíku a pokud
     *  se některý z názvů věcí shoduje s názvem zadané
     *  věci, metoda vrátí tuto věc. 
     *
     *  @param parametry - jako  parametr obsahuje název věci
     *
     *  @return nalezena nebo null
     */ 
    public Vec vratVec(String nazev){
        Vec nalezena = null;
        for(Vec vec : seznamVeci){
            if(vec.vratNazev().equals(nazev)){
                nalezena = vec;
                break;
            }
        }
        return nalezena;
    }
       
    /**
     *  Metoda, která odstraní ze seznamu zadanou věc, 
     *  pokud se v něm skutečně nachází a vrátí 
     *  odstraněnou věc.
     *
     *  @param parametry - jako  parametr obsahuje název věci
     *
     *  @return vec nebo null
     */ 
        
    public Vec vyndatVec(String nazev) {
        Vec vec = vratVec(nazev);
        if(vec != null) {
            seznamVeci.remove(vec);
            return vec;
        } else {
            return null;
        }
    }

    //== Soukromé metody (instancí i třídy) ========================================
    /**
     *  Metoda sečte hmotnost věcí ve vozíku.
     *
     *  @return celkovaHmotnost
     */ 

    public int spocitejHmotnost() {
        int celkovaHmotnost = 0;
        for(Vec vec : seznamVeci) {
            celkovaHmotnost = celkovaHmotnost + vec.vratHmotnost();
        }
        return celkovaHmotnost;
    }
}
