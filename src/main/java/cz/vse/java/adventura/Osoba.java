/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package cz.vse.java.adventura;



/*******************************************************************************
 * Instance třídy Osoba představují ...
 *
 * @author   Barbora Vimmerová
 * @version   0.00.000
 */
public class Osoba
{
    //== Datové atributy (statické i instancí)======================================
    private String jmeno;
    private String rada;

    //== Konstruktory a tovární metody =============================================

    /***************************************************************************
     *  Konstruktor třídy
     */
    public Osoba(String jmeno, String rada)
    {
        this.jmeno = jmeno;
        this.rada = rada;
    }
    
     /**
     *  Metoda vrací jméno osoby.
     */
    public String vratJmeno(){
        return jmeno;
    }
    
     /**
     *  Metoda vrací radu, kterou osoba poskytuje.
     */
    public String vratRadu(){
        return rada;
    }

    //== Nesoukromé metody (instancí i třídy) ======================================


    //== Soukromé metody (instancí i třídy) ========================================

}
