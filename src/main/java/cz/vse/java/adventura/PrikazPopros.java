/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package cz.vse.java.adventura;



/*******************************************************************************
 * Instance třídy PrikazPopros představují ...
 *
 * @author    Barbora Vimmerová
 * @version   0.00.000
 */
public class PrikazPopros implements IPrikaz
{
    //== Datové atributy (statické i instancí)======================================
    private static String NAZEV = "popros";
    private HerniPlan plan;

    //== Konstruktory a tovární metody =============================================

    /***************************************************************************
    *  Konstruktor třídy
    *  
    *  @param plan herní plán, ve kterém se bude ve hře "chodit"
    */
    public PrikazPopros(HerniPlan plan)
    {  
        this.plan = plan;
    }
    
    
    /**
     *  Provádí příkaz "popros". Zjistí, která osoba se nachází v 
     *  dané místnosti a vypíše její radu.
     *
     *  @return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String provedPrikaz(String... parametry) {

        //== Nesoukromé metody (instancí i třídy) ======================================
        Prostor aktualniProstor = plan.vratAktualniProstor();
        Osoba osobaS = aktualniProstor.vratOsobu();
        return osobaS.vratRadu();
    }
    

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    
    @Override
    public String getNazev() {
        return NAZEV;
    }
    


    //== Soukromé metody (instancí i třídy) ========================================

}
