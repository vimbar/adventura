package cz.vse.java.adventura;

import java.util.*;

/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 */
public class HerniPlan {
    
    private Prostor aktualniProstor;
    private Prostor viteznyProstor;
    private Vozik vozik;
    private List<Prostor> seznamProstoru;
    private int pocetNavstivenychProstoru = 0;
    
     /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví duty free shop.
     */
    public HerniPlan() {
        vozik = new Vozik();
        seznamProstoru = new ArrayList();
        zalozProstoryHry();
    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví duty free shop.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
        Prostor odbavovnaZavazadel = new Prostor("odbavovna","odbavovna, kde lidé odbavují kufry");
        Prostor uschovnaZavazadel  = new Prostor("uschovna", "úschovna zavazadel, kde si lze na pár hodin uschovat zavazadla ve skříňkách");
        Prostor dutyFreeShop  = new Prostor("shop","duty free shop, kde si lze něco výhodně nakoupit");
        Prostor celnice = new Prostor("celnice","celnice, kde vás prověří celník, pokud jste podezřelí cestující");
        Prostor checkIn = new Prostor("checkin","check-in, místo, kde ukazujete pas a potvrzujete, že jste dorazili na letiště");
        Prostor toalety  = new Prostor("toalety","toalety, kam si můžete odskočit");
        Prostor gate1 = new Prostor("gate1","gate1,jeden ze tří gatů, v tom správném odevzdejte letenku");
        Prostor gate2  = new Prostor("gate2","gate2 ,jeden ze tří gatů, v tom správném odevzdejte letenku");
        Prostor gate3 = new Prostor("gate3","gate 3,jeden ze tří gatů, v tom správném odevzdejte letenku");
        Prostor restaurace  = new Prostor("restaurace","restaurace, kde se můžete občerstvit");
        
        seznamProstoru.add(odbavovnaZavazadel);
        seznamProstoru.add(uschovnaZavazadel);
        seznamProstoru.add(dutyFreeShop);
        seznamProstoru.add(celnice);
        seznamProstoru.add(checkIn);
        seznamProstoru.add(toalety);
        seznamProstoru.add(gate1);
        seznamProstoru.add(gate2);
        seznamProstoru.add(gate3);
        seznamProstoru.add(restaurace);
        
        // přiřazují se průchody mezi prostory (sousedící prostory)
        odbavovnaZavazadel.setVychod(gate1);
        odbavovnaZavazadel.setVychod(celnice);
        odbavovnaZavazadel.setVychod(uschovnaZavazadel);
        uschovnaZavazadel.setVychod(odbavovnaZavazadel);
        uschovnaZavazadel.setVychod(dutyFreeShop);
        uschovnaZavazadel.setVychod(checkIn);
        dutyFreeShop.setVychod(restaurace);
        dutyFreeShop.setVychod(celnice);
        dutyFreeShop.setVychod(toalety);
        dutyFreeShop.setVychod(uschovnaZavazadel);
        celnice.setVychod(odbavovnaZavazadel);
        celnice.setVychod(dutyFreeShop);
        celnice.setVychod(gate2);
        checkIn.setVychod(toalety);
        checkIn.setVychod(uschovnaZavazadel);
        toalety.setVychod(dutyFreeShop);
        toalety.setVychod(checkIn);
        gate1.setVychod(gate2);
        gate1.setVychod(odbavovnaZavazadel);
        gate2.setVychod(gate1);
        gate2.setVychod(celnice);
        gate2.setVychod(gate3);
        gate3.setVychod(gate2);
        restaurace.setVychod(dutyFreeShop);
                
        aktualniProstor = uschovnaZavazadel;  // hra začíná v duty-free shopu
        
        //vytvoření jednotlivých věcí
        Vec letenka = new Vec("letenka",1);
        Vec pas = new Vec("pas",1);
        Vec voucher1 = new Vec("voucher1",1);
        Vec kufr = new Vec("kufr",18);
        Vec voucher2 = new Vec("voucher2", 1);
        Vec voucher3 = new Vec("voucher3",1);
        
        //vytvoření jednotlivých osob
        Osoba celnik = new Osoba("celník","Ukaž mi rychle pas.");
        Osoba letuska = new Osoba("letuška","V duty free shopu jsem zahlédla nějaký voucher.");
        Osoba pilot = new Osoba("pilot","Dávejte si pozor, v některé z vedlejších místností je celník.");
        Osoba informacniAsistent = new Osoba("informační asistent","Do gatů bez letenky ani nezkoušejte chodit.");
        
        
        
        //vložení věcí do prostorů
        toalety.vlozVec(kufr);
        odbavovnaZavazadel.vlozVec(pas);
        restaurace.vlozVec(voucher1);
        uschovnaZavazadel.vlozVec(voucher2);
        dutyFreeShop.vlozVec(letenka);
        dutyFreeShop.vlozVec(voucher3);
        
        //nastaví cílovou věc pro daný prostor
        checkIn.nastavCilovouVec(pas);
        odbavovnaZavazadel.nastavCilovouVec(kufr);
        gate3.nastavCilovouVec(letenka);
        
        //vloží osobu do prostoru
        toalety.priradOsobu(letuska);
        uschovnaZavazadel.priradOsobu(celnik);
        odbavovnaZavazadel.priradOsobu(pilot);
        checkIn.priradOsobu(informacniAsistent);
        
        
        
    }
    
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor vratAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void nastavAktualniProstor(Prostor prostor) {
       aktualniProstor = prostor;
    }
  
    
    public boolean jeVyhra(){
        for(Prostor prostor : seznamProstoru) {
            if(!prostor.maCilovouVec()) {
                return false;
            }
        }
        return true;
    }
    
    public boolean jeProhra() {
        if(pocetNavstivenychProstoru > 30) {
            return true;
        }
        
        return false;
    }
    
    public Vozik vratVozik() {
        return vozik;
    }
    
    public void noveNavstivenyProstor() {
        pocetNavstivenychProstoru++;
    }
    
    public void zmenPocetNavstivenychProstoru(int hodnota) {
        pocetNavstivenychProstoru = pocetNavstivenychProstoru + hodnota;
    }

}
