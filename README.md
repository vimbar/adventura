Autor: Barbora Vimmerová
Verze: 1 - vytvořeno v BlueJ 3.1.0
Datum: 26.6.2019

Popis: Jedná se o jednoduchou adventuru v prostředí letiště, kdy cílem hráče je najít pomocí příkazů: jdi, 
prozkoumej a seber tři předměty v daných prostorách: kufr, letenku a pas a převést je pomocí seznamu 
Vozík do správného prostoru, pro který je daný předmět cílovou věcí, tam je pomocí příkazu odevzdej vyndat ze 
seznamu. Pokud prostory odbavovnaZavazadel(kufr), check-in(pas) a gate určený hráči na začátku(letenka) obsahují 
své cílové věci uvedené v závorkách, hra končí a znamená to, že hráč vyhrál. Hráč musí však stihnout 
věci přemístit během pouhých 30 vstupů do prostorů (hra obsahuje počítadlo vstupů = prakticky počítado použití 
příkazu jdi), přičemž, pokud vstupuje do stejného prostoru podruhé počítá se to jako další vstup. Počet vstupů se 
hráči navýší, pakliže najde a sebere do vozíku voucher, každý voucher v batohu jednorázově sníží počet napočítaných 
vstupů o 5 vouchery se sčítají. Hráč unese pouzev vozík o hmotnosti 20kg a hmotnosti věcí se sčítají.  
       
7 příkazů: nápověda, konec, jdi, seber, popros, odevzdej, prozkoumej
další třídy: Vec, Oosba, Prostor, Vozík, Hra, HerniPlan
       